Source: python-djantic
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 mkdocs (>= 1.0.4),
 mkdocs-material,
 pybuild-plugin-pyproject,
 python3-all,
 python3-django,
 python3-factory-boy,
 python3-mkautodoc,
 python3-poetry-core,
 python3-psycopg2,
 python3-pydantic,
 python3-pytest,
 python3-pytest-django,
 python3-sphinx,
Standards-Version: 4.6.2
Homepage: https://github.com/jordaneremieff/djantic
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-djantic
Vcs-Git: https://salsa.debian.org/python-team/packages/python-djantic.git
Rules-Requires-Root: no

Package: python-djantic-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${mkdocs:Depends},
Description: Pydantic model support for Django (Documentation)
 Djantic is a library that provides a configurable utility class for
 automatically creating a Pydantic model instance for any Django model class. It
 is intended to support all of the underlying Pydantic model functionality such
 as JSON schema generation and introduces custom behaviour for exporting Django
 model instance data.
 .
 This package contains the documentation.

Package: python3-djantic
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-djantic-doc,
Description: Pydantic model support for Django (Python3 version)
 Djantic is a library that provides a configurable utility class for
 automatically creating a Pydantic model instance for any Django model class. It
 is intended to support all of the underlying Pydantic model functionality such
 as JSON schema generation and introduces custom behaviour for exporting Django
 model instance data.
 .
 This package contains the Python 3 version of the library.
